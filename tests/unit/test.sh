#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Runs upstream tests (they are not part of the source tarball)
# make sure to update VERSION_TAG in the case of rebase

# Autodetection from rpm if not set directly
# VERSION_TAG="${VERSION_TAG:-v1.1.0}"

PYTHON="${PYTHON:-python3}"
USER="tester$$"
USER_HOME="/home/$USER"

UPSTREAM_SOURCE="https://github.com/PyMySQL/PyMySQL"

rlJournalStart
    rlPhaseStartSetup
        rlImport "database/mariadb" || rlDie
        rlRun mariadbStart || rlDie

        rlRun "mysql -uroot -e \"SET PASSWORD FOR 'root'@'localhost' = '' \""
        rlRun "mysql -h127.0.0.1 -uroot -e 'select version()'" || rlDie

        rlRun "useradd $USER"
        git clone $UPSTREAM_SOURCE "$USER_HOME/repo"
        SYSTEM_LOCATION="$(dirname $($PYTHON -c 'import pymysql; print(pymysql.__file__)'))"
        if [[ -z "$VERSION_TAG" ]]; then
            VERSION_TAG="v$(rpm -qf $SYSTEM_LOCATION --qf '%{VERSION}')"
            rlLog "Detected version: $VERSION_TAG"
        fi
        rlRun "(cd $USER_HOME/repo && git checkout $VERSION_TAG)"
        rlRun "rlFileBackup --clean $SYSTEM_LOCATION"
        rlRun "mv $USER_HOME/repo/pymysql/tests $SYSTEM_LOCATION"

        rlRun 'mysql -h127.0.0.1 -uroot -e "SET GLOBAL local_infile=on"'
        rlRun "mysql -h127.0.0.1 -uroot --comments < $USER_HOME/repo/ci/docker-entrypoint-initdb.d/init.sql"
        rlRun "mysql -h127.0.0.1 -uroot --comments < $USER_HOME/repo/ci/docker-entrypoint-initdb.d/mysql.sql"
        rlRun "mysql -h127.0.0.1 -uroot --comments < $USER_HOME/repo/ci/docker-entrypoint-initdb.d/mariadb.sql"
        rlRun "cp $USER_HOME/repo/ci/docker.json $SYSTEM_LOCATION/tests/databases.json"

        rlRun "sed -e 's^/run/mysqld/mysqld.sock^/var/lib/mysql/mysql.sock^' -i $SYSTEM_LOCATION/tests/databases.json"
    rlPhaseEnd

    rlPhaseStartTest
        # there is no `nacl` on rhel/c10s
        rlRun "su -l -c \"pytest -v $SYSTEM_LOCATION/tests -k 'not testAuthEd25519'\" $USER"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun mariadbStop
        rlRun "userdel -rf $USER"
    rlPhaseEnd
rlJournalEnd
